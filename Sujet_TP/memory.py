import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    '''

    Parameters
    ----------
    Tab : List
        Cette liste contient les 6 paires que le joueur va devoir retrouver lors de sa partie du jeu du mémory . 

    Returns
    -------
    Tab : List
        Renvoie une liste contenant les 12 pièces du jeu dans un ordre aléatoire.

    '''
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[j], Tab[i]
    return Tab

def carte_cache(Tab):   #TODO
    """
    
    Parameters
    ----------
    Tab:List
        Cette liste contient les 12 pièces du jeu rangé chacune à côté de sa paire et dans l'ordre alphabétique
        
     Returns
     -------
     Tab : list
         On obtient une liste de même longeur que la liste que renvoie melange_carte qui renvoie le dos des cartes
        """
        
        
        
    """TODO
        Doit créer une liste de même longueur que la liste renvoyés dans la question précédentes
    """
    T=[0 for k in range (len(melange_carte(Tab)))]
    return T

def choisir_cartes(Tab):
    """
    Parameters
    ----------
    Tab : Liste contenant les 6 paires que le joueur devra retrouver

    Returns
    -------
    c1,c2 : les deux cartes choisies par le joueur 
    """
    c1 = int(input("Choisissez une carte : "))
    if c1 < 1 or c1 > len(Tab):
        print("Erreur, la carte choisie est hors jeu ! ")
        return choisir_cartes(Tab)
    print(Tab[c1-1])
    c2 = int(input("Choisissez une deuxieme carte : "))
    if c2 < 1 or c2 > len(Tab) or c2 == c1:
        print("Erreur, la carte choisie est hors jeu ou identique à la première ! ")
        return choisir_cartes(Tab)
    print(Tab[c2-1])
    return [c1-1 , c2-1]



def retourne_carte(c1, c2, Tab, Tab_cache):
    """
    Parameters
    ----------
    
    Tab : Liste contenant les 6 paires que le joueur devra retrouver
    c1, c2 : les deux cartes choisies par le joueur
    Tab_cache : liste contenant les cartes "de dos"

    Returns
    -------
    Tab_cache : liste affichant les deux cartes choisies retournées

    """
    
    Tab_cache[c1],Tab_cache[c2] = Tab[c1],Tab[c2]
    return (Tab_cache)

def jouer(Tab):
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)
    "On affiche le jeu avec les carte cachée"

    [c1, c2] = choisir_cartes(Tab)
    "Le jouer choisi 2 cartes"
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache) 
    print(Tab_cache) 
    "On affiche le jeu avec les carte retournée"
    
    while 0 in Tab_cache:
        "Tant qu'il reste un 0 dans le jeu on continue de faire choisir 2 cartes au joueur"
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        "on renvoi le jeu avec les 2 cartes retourné en plus"
        print(Tab_cache)
    "si toute les cartes sont retournées le jeu se termine"
    print("Bravo tu as gagné!!!")

    
jouer(Tabl)

if __name__ == "__main__":
        
    #-- QUESTION 1 --#
    print('\n Test unitaire melange_carte')
    print('Expected:\n')
    print(melange_carte(Tabl))
    print('\n Get:')
    print(melange_carte(Tabl)) #test unitaire avec un random, je sais pas comment faire 
    
        #-- QUESTION 2 --#
    print('\n Test unitaire choisir cartes')
    print('Expected:\choisir_cartes(Tab)\nGet:')
    print(choisir_cartes(Tabl))
    
    
    
    
    
    